package com.lviv.hv.accounting.service;

import com.lviv.hv.accounting.security.UserAuthentication;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class TokenAuthService {

  private static final String AUTH_HEADER_NAME = "X-Auth-Token";

  private static final String AUTH = "/authentication";
  private static final String SLASH = "/";

  @Value("${accounting.service.url}")
  private String accountingServiceUrl;

  private final RestTemplate restTemplate = new RestTemplate();

  public Optional<UserAuthentication> getAuthentication(HttpServletRequest request){
    String url = accountingServiceUrl + AUTH;
    MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    headers.add(AUTH_HEADER_NAME, request.getHeader(AUTH_HEADER_NAME));
    HttpEntity<String> httpEntity = new HttpEntity<String>(null, headers);
    UserAuthentication userAuthentication = this.restTemplate.exchange(url, HttpMethod.GET, httpEntity, UserAuthentication.class).getBody();
    return Optional.ofNullable(userAuthentication);
  }

}
