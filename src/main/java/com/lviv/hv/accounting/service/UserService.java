package com.lviv.hv.accounting.service;

import com.lviv.hv.accounting.domain.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class UserService {
    private static final String USER = "/user";
    private static final String SLASH = "/";

    @Value("${accounting.service.url}")
    private String accountingServiceUrl;

    private final RestTemplate restTemplate = new RestTemplate();

    public List<User> getAllUsers(){
        String url = accountingServiceUrl + USER;
        HttpEntity<String> request = new HttpEntity<>(null, null);
        List<User> users = this.restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<User>>() { }).getBody();
        return users;
    }

    public User getUser(long id) {
        String url = accountingServiceUrl + USER + SLASH + id;
        HttpEntity<String> request = new HttpEntity<>(null, null);
        return this.restTemplate.exchange(url, HttpMethod.GET, request, User.class).getBody();
    }

}
