package com.lviv.hv.accounting.security;

import com.lviv.hv.accounting.domain.Account;
import com.lviv.hv.accounting.domain.Role;
import java.util.List;
import lombok.NonNull;
import org.springframework.security.core.Authentication;

public class UserAuthentication implements Authentication {

  private Account account;

  public UserAuthentication(){
    super();
  }

  public UserAuthentication(@NonNull Account account) {
    this.account = account;
  }

  private boolean authenticated = true;

  @Override
  public List<Role> getAuthorities() {
    return account.getAuthorities();
  }

  @Override
  public Object getCredentials() {
    return account.getPassword();
  }

  @Override
  public Object getDetails() {
    return account;
  }

  @Override
  public Object getPrincipal() {
    return account.getLogin();
  }

  @Override
  public boolean isAuthenticated() {
    return authenticated;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    this.authenticated = isAuthenticated;
  }

  @Override
  public String getName() {
    return account.getLogin();
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }
}
