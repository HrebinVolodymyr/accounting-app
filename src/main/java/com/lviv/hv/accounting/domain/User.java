package com.lviv.hv.accounting.domain;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"purchases"})
public class User {

  private long id;
  private String firstName;
  private String lastName;
  private String email;
  private List<Purchase> purchases;
}
