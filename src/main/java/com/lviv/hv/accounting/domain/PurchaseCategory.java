package com.lviv.hv.accounting.domain;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "purchases")
public class PurchaseCategory {

  private long id;
  private String name;
  private String description;
  private List<Purchase> purchases;
}
