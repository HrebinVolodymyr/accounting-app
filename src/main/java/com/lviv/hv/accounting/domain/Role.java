package com.lviv.hv.accounting.domain;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

@Data
public class Role implements GrantedAuthority
{

    private long id;
    private String name;

    @Override
    public String getAuthority() {
        return name;
    }
}
