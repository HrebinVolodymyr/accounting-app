package com.lviv.hv.accounting.controller;

import com.lviv.hv.accounting.domain.User;
import com.lviv.hv.accounting.service.UserService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
public class UserController {

    @Autowired
    private final UserService userService;

    public UserController(UserService userService){
        super();
        this.userService = userService;
    }

    @GetMapping(value={"/", "/index"})
    public String getHomePage(Model model){

        return "index";
    }

    @GetMapping(value="/login")
    public String getLoginPage(Model model){
        return "login";
    }

    @GetMapping(value="/logout-success")
    public String getLogoutPage(Model model){
        return "logout";
    }

    @GetMapping(value="/user")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public String getGuests(HttpServletRequest request, Model model){
        List<User> users = this.userService.getAllUsers();
        model.addAttribute("users", users);
        return "users-view";
    }

    @GetMapping(value="/user/add")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getAddGuestForm(Model model){
        return "user-view";
    }

    @GetMapping(value="/user/{id}")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public String getGuest(Model model, @PathVariable long id){
        User user = this.userService.getUser(id);
        model.addAttribute("user", user);
        return "user-view";
    }

}
